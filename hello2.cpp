///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World C++
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello2.cpp
/// @version 1.0
///
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {

   std::cout << "Hello World!" << std::endl;

   return 0;
}

