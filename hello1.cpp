///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World C++
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello1.cpp
/// @version 1.0
///
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

int main() {

   cout << "Hello World!" << endl;

   return 0;
}

